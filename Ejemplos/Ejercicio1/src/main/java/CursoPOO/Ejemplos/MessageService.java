package CursoPOO.Ejemplos;

/**
 * Created by jorge.capel on 10/10/2016.
 */
public interface MessageService {

    String getMessage();
}
