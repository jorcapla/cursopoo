package CursoPOO.Ejemplos;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


/**
 * Hello world!
 *
 */

@SpringBootApplication
@ComponentScan
public class App 
{
    public static void main( String[] args )
    {

        SpringApplication.run(App.class,args);

    }
    @Bean
    public CommandLineRunner demo(MessagePrinter messagePrinter) {
        return (args) -> {
            messagePrinter.print();
        };
    }
}
