package hello.domain;

/**
 * Created by jorge.capel on 06/10/2016.
 */

public interface MessageService {
        String getMessage();
}

