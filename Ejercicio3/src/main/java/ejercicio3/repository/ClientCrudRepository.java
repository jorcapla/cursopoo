package ejercicio3.repository;

import ejercicio3.domain.Client;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by jorge.capel on 09/10/2016.
 */
public interface ClientCrudRepository extends CrudRepository<Client, Long> {
}
