package CursoPOO.ejemplos.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jorge.capel on 10/10/2016.
 */
@RestController
public class HelloController {

    @RequestMapping(name = "/hello")
    public String hello(){
        return "Hola mundo";
    }

}
