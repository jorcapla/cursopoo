package hello.domain;

/**
 * Created by jorge.capel on 06/10/2016.
 */

import org.springframework.beans.factory.annotation.Autowired;




public class MessagePrinter {

    final private MessageService service;


    public MessagePrinter(MessageService service) {
        this.service = service;
    }

    public void printMessage() {
         System.out.println(this.service.getMessage());
    }
}