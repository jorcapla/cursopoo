package ejercicio3.controller;


import ejercicio3.domain.Client;
import ejercicio3.domain.Order;
import ejercicio3.services.ClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jorge.capel on 09/10/2016.
 */

@RestController
@RequestMapping("/clients")
public class ClientController {


    private ClientsService clientsService;

    @RequestMapping( method = RequestMethod.GET)
    Iterable<ejercicio3.domain.Client> rearUsers() {

        return this.clientsService.findAll();
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    ejercicio3.domain.Client readUser(@PathVariable long userId) {

        return this.clientsService.findOne(userId);
    }
    @RequestMapping(value = "/{userId}/orders", method = RequestMethod.GET)
    Iterable<ejercicio3.domain.Order> readOrders(@PathVariable long userId) {
        List<Order> orderList = new ArrayList<Order>();
        Client client = this.clientsService.findOne(userId);
        if (client!=null){
            orderList.addAll(client.getOrders());
        }
        return orderList;
    }
    @Autowired
    ClientController(ClientsService clientsService) {
        this.clientsService = clientsService;

    }

}
