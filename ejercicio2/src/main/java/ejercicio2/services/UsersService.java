package ejercicio2.services;

import ejercicio2.domain.User;
import ejercicio2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by jorge.capel on 09/10/2016.
 */

@Service
public class UsersService {

    @Autowired
    private UserRepository userRepository;


    public Collection<User> findAll() {

        return userRepository.findAll();
    }

    public User findOne(String userId) {

        return userRepository.findAll().get(0);
    }
}
