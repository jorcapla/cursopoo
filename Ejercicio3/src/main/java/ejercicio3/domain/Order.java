package ejercicio3.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by jorge.capel on 09/10/2016.
 */
@Entity(name="orders")

public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long  id;
    private Date birthDate;
    private double total;

    @JsonBackReference
    @ManyToOne( fetch =FetchType.LAZY)
    @JoinColumn(name="client_id")
    private Client client;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
