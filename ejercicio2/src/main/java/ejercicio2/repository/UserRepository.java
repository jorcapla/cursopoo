package ejercicio2.repository;

import ejercicio2.domain.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorge.capel on 09/10/2016.
 */

@Repository
public class UserRepository {

    public List<User> findAll(){

        List<User> userList = new ArrayList<User>();

        User user1 = new User();
        user1.setId((long)1.0);
        user1.setFirstName("Jorge");
        user1.setLastName("Capel Planells");
        user1.setName(user1.getFirstName()+" "+user1.getLastName());
        userList.add(user1);

        User user2 = new User();
        user2.setId((long)2.0);
        user2.setFirstName("JAvier");
        user2.setLastName("Capel Planells");
        user2.setName(user2.getFirstName()+" "+user2.getLastName());
        userList.add(user2);



        return  userList;

    }
}
