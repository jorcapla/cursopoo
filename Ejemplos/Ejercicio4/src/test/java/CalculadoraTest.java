import org.junit.Test;


import static junit.framework.Assert.*;

/**
 * Created by jorge.capel on 17/10/2016.
 */

public class CalculadoraTest {

    @Test
    public void el_resultado_de_sumar_los_numeros_2_mas_3_debe_ser_5() throws Exception {
        //Arrange
        int resultadoSuma = 5;
        Calculadora calculadora = new Calculadora();
        //Act
        int suma = calculadora.suma(2, 3);
        //Assert
        assertTrue("La suma de 2+3 debe ser 5",resultadoSuma == suma);
    }
    @Test
    public void el_resultado_de_dividir_los_numeros_6_entre_3_debe_ser_2() throws Exception {
        //Arrange
        double resultadoDivision = 2.0;
        Calculadora calculadora = new Calculadora();
        //Act
        double division = calculadora.dividir(6, 3);
        //Assert
        assertEquals("El resultado de dividir 6 entre 3 debe ser 2",resultadoDivision,division);
    }
    @Test(expected = ArithmeticException.class)
    public void debe_lanzar_excepcion_x_cuando_dividimos_entre_cero()  {
        //Arrange
        double resultadoDivision = 0;
        Calculadora calculadora = new Calculadora();
        //Act
        double division = calculadora.dividir(6, 0);
        //Assert
      //  assertEquals("El resultado de dividir 6 entre 0 debe ser infinito",resultadoDivision,division);
    }
}