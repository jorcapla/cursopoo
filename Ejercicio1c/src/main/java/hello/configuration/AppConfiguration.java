package hello.configuration;

import hello.domain.EnglishMessageService;
import hello.domain.MessageService;
import hello.domain.SpanishMessageService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jorge.capel on 06/10/2016.
 */
@Configuration
@ComponentScan
public class AppConfiguration {

    @Bean(name="english")
    MessageService HelloWorldMessageService() {
        return new EnglishMessageService();

    }
    @Bean(name = "espanol")
    MessageService HolaMundoMessageService() {
        return new SpanishMessageService();
    }
}
