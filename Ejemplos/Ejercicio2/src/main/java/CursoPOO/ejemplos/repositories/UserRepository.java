package CursoPOO.ejemplos.repositories;

import CursoPOO.ejemplos.domain.User;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jorge.capel on 10/10/2016.
 */
@Repository
public class UserRepository {

    private List<User> userList = null;

    public List<User> findAll(){
       return userList;
    }

    public UserRepository(){

        userList = new ArrayList<User>();
        User user1 = getNewUser();
        userList.add(user1);

        User user2 = getNewUser();
        user2.setId((long)2);
        userList.add(user2);
    }
    private  User getNewUser() {

        User user = new User();
        user.setId((long)1.0);
        user.setFirstName("Jorge");
        user.setLastName("Capel");
        user.setName(user.getFirstName()+' '+user.getLastName());
        user.setBirthDate(getFecha());
        return user;
    }

    private Date getFecha() {

        SimpleDateFormat dateformat3 = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = null;
        try {
            date1 = dateformat3.parse("17/07/1989");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1;
    }

    public User findOne(String userId) {
      return userList.get(0);
    }
}
