package hello.domain;

/**
 * Created by jorge.capel on 06/10/2016.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component
public class MessagePrinter {

    final private MessageService service;

    @Autowired
    public MessagePrinter(@Qualifier("english") MessageService service) {
        this.service = service;
    }

    public void printMessage() {
         System.out.println(this.service.getMessage());
    }
}