package ejercicio3;


import ejercicio3.services.ClientsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jorge.capel on 09/10/2016.
 */

@Configuration
public class AppConfiguration {


    @Bean
    public ClientsService usersService(){

        return new ClientsService();
    }
}
