package CursoPOO.Ejemplos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by jorge.capel on 10/10/2016.
 */
@Component
public class MessagePrinter {

    private final MessageService messageService;

    @Autowired
    public MessagePrinter(@Qualifier("Spanish") MessageService messageService) {
        this.messageService = messageService;
    }

    public void print(){

        String message = messageService.getMessage();
        System.out.println(message);
    }

}
