package hello;

/**
 * Created by jorge.capel on 06/10/2016.
 */
import hello.domain.MessagePrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
@ComponentScan
public class Application {


    public static void main(String[] args) {

        SpringApplication.run(Application.class,args);
    }
    @Bean
    public CommandLineRunner demo( MessagePrinter messagePrinter) {
        return (args) -> {
            messagePrinter.printMessage();
        };
    }
}