package ejercicio3.services;



import ejercicio3.repository.ClientCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by jorge.capel on 09/10/2016.
 */

@Service
public class ClientsService {

    @Autowired
    private ClientCrudRepository clientRepository;


    public Iterable<ejercicio3.domain.Client> findAll() {

        return clientRepository.findAll();
    }

    public ejercicio3.domain.Client findOne(long userId) {

        return clientRepository.findOne(userId);
    }
}
