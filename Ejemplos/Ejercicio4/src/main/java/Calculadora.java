/**
 * Created by jorge.capel on 17/10/2016.
 */

public class Calculadora {

    public int suma( int a, int b){
        return  a+b;
    }
    public double dividir(double a, double b) throws ArithmeticException{

        if (b==0) {throw new ArithmeticException();}
        return a/b;
    }
}
