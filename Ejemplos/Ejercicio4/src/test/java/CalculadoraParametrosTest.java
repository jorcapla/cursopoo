import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import static junit.framework.Assert.*;

/**
 * Created by jorge.capel on 17/10/2016.
 */
@RunWith(Parameterized.class)
public class CalculadoraParametrosTest {
    private int fInputA;
    private int fInputB;
    private int fExpected;
    private static Calculadora  fCalculadora;

    @BeforeClass
    public static void setUpClass() {
        fCalculadora= new Calculadora();
    }
    @AfterClass
    public static void TearDownClass() {
        fCalculadora= null;
    }
    @Before
    public void setUp() {
        System.out.println("@Before setUp " + Integer.toString(this.fInputA));
    }
    @After
    public void tearDown() {
        System.out.println("@After setUp " + Integer.toString(this.fInputA));
    }
        @Parameterized.Parameters(name = "La suma de ({0},{1})={2}")
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][] {
                    { 0, 0, 0 },
                    { 1, 1 ,2},
                    { 2, 1 ,3},
                    { 3, 2 ,5},
                    { 4, 3 ,7},
                    { 5, 5 ,10},
                    { 6, 8 ,14}
            });
        }




    public CalculadoraParametrosTest(int inputA,int inputB, int expected) {
            fInputA= inputA;
            fInputB = inputB;
            fExpected= expected;
        }

        @Test
        public void sumar() {
            assertEquals(fExpected, fCalculadora.suma(fInputA,fInputB));
        }
    }

