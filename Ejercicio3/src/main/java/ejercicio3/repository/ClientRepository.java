package ejercicio3.repository;

import ejercicio3.domain.Client;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorge.capel on 09/10/2016.
 */

@Repository
public class ClientRepository {

    public List<Client> findAll(){

        List<Client> clientList = new ArrayList<Client>();

        Client client1 = new Client();
        client1.setId((long)1.0);
        client1.setFirstName("Jorge");
        client1.setLastName("Capel Planells");
        client1.setName(client1.getFirstName()+" "+ client1.getLastName());
        clientList.add(client1);

        Client client2 = new Client();
        client2.setId((long)2.0);
        client2.setFirstName("JAvier");
        client2.setLastName("Capel Planells");
        client2.setName(client2.getFirstName()+" "+ client2.getLastName());
        clientList.add(client2);



        return clientList;

    }
}
