package ejercicio2.controller;

import ejercicio2.domain.User;
import ejercicio2.repository.UserRepository;
import ejercicio2.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * Created by jorge.capel on 09/10/2016.
 */

@RestController
@RequestMapping("/users")
public class UserController {


    private UsersService usersService;

    @RequestMapping( method = RequestMethod.GET)
    Collection<User> rearUsers() {

        return this.usersService.findAll();
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    User readUser(@PathVariable String userId) {

        return this.usersService.findOne(userId);
    }
    @Autowired
    UserController(UsersService usersService) {
        this.usersService = usersService;

    }

}
