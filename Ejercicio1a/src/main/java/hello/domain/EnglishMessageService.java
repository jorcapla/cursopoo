package hello.domain;

/**
 * Created by jorge.capel on 06/10/2016.
 */
public class EnglishMessageService implements MessageService {
    public String getMessage() {
        return "Hello world";
    }
}
