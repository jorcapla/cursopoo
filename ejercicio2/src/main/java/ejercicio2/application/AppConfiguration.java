package ejercicio2.application;

import ejercicio2.repository.UserRepository;
import ejercicio2.services.UsersService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jorge.capel on 09/10/2016.
 */
@ComponentScan("ejercicio2.*")
@Configuration
public class AppConfiguration {

    @Bean
    public UserRepository userRepository(){

        return new UserRepository();
    }
    @Bean
    public UsersService usersService(){

        return new UsersService();
    }
}
