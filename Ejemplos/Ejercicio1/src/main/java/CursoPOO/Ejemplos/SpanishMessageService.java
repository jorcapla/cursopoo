package CursoPOO.Ejemplos;

import org.springframework.stereotype.Component;

/**
 * Created by jorge.capel on 10/10/2016.
 */

@Component(value="Spanish")
public class SpanishMessageService implements MessageService {
    public String getMessage() {
        return "Hola mundo!";
    }
}
