package CursoPOO.ejemplos.controllers;

import CursoPOO.ejemplos.domain.Customer;
import CursoPOO.ejemplos.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jorge.capel on 10/10/2016.
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    Iterable<Customer> getCustomers(){

        return customerRepository.findAll();
    }
}
