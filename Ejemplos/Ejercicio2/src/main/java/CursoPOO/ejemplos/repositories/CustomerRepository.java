package CursoPOO.ejemplos.repositories;

/**
 * Created by jorge.capel on 10/10/2016.
 */
import java.util.List;

import CursoPOO.ejemplos.domain.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);
}
