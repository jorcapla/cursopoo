package CursoPOO.ejemplos.services;

import CursoPOO.ejemplos.domain.User;
import CursoPOO.ejemplos.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jorge.capel on 10/10/2016.
 */
@Service
public class UsersServices {

    private final UserRepository userRepository;

    @Autowired
    public UsersServices(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(String userId) {
        return userRepository.findOne(userId);
    }
}
