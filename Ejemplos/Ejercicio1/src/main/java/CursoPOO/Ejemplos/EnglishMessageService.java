package CursoPOO.Ejemplos;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by jorge.capel on 10/10/2016.
 */

@Component(value="english")
public class EnglishMessageService implements MessageService {
    public String getMessage() {
        return "Hello world!";
    }
}
