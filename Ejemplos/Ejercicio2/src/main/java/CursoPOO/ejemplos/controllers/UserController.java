package CursoPOO.ejemplos.controllers;

import CursoPOO.ejemplos.domain.User;
import CursoPOO.ejemplos.services.UsersServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by jorge.capel on 10/10/2016.
 */
@RestController
@RequestMapping(value ="/users")
public class UserController {

    private final UsersServices usersServices;

    @Autowired
    public UserController(UsersServices usersServices) {
        this.usersServices = usersServices;
    }


    @RequestMapping(value="/{userId}",method = RequestMethod.GET)
    User getUser(@PathVariable String userId){

        return usersServices.findOne(userId);
    }

    @RequestMapping(method = RequestMethod.GET)
    List<User> getUsers(){



        return usersServices.findAll();
    }


}
