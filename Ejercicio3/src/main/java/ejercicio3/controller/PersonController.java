package ejercicio3.controller;

import ejercicio3.domain.Person;
import ejercicio3.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by jorge.capel on 06/10/2016.
 */
@RestController
public class PersonController {

    PersonRepository personRepository;

    @RequestMapping("/people")
    public List<Person> people(@RequestParam(value="name", defaultValue="World") String name) {
        return personRepository.findByLastName(name);
    }
    @Autowired
    PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;

    }
}
